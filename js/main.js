// An example polar clock.

// Event callback when document is ready.
$( document ).ready( function() {
	
	// Define a timer to refresh the clock.
	window.setInterval(function() {

		// Grab the canvas
		var canvas = document.getElementById('clock_canvas')

		canvas.width = $(window).width();
		canvas.height = $(window).height();

		// Establish Context
		var ctx = canvas.getContext('2d');

		// Define required cords for centering
		var c_x = canvas.width / 2;
		var c_y = canvas.height / 2;

		// Get the current date
		var d = new Date();

		var clock = [
						[d.getMonth(), 12, "Months"],
						[d.getDate(), 31, "Days"],
						[d.getHours(), 24, "Hours"],
						[d.getMinutes(), 60, "Minutes"],
						[d.getSeconds(), 60, "Seconds"]
					];

		$('#clock_text').empty();

		for ( var i = 0; i < clock.length; i++) {

			var text_class = (i % 2 == 0 ) ? "dark" : "light";
			
			$('#clock_text').append( ' <span class="' + text_class + '">'  + clock[i][0] + " " + clock[i][2] + "</span>")

			ctx.beginPath();
			ctx.arc(
				c_x,
				c_y,
				((canvas.height / 3) * (1 / clock.length)) * (i + 1),
				1.5 * Math.PI,
				get_circle_fraction(clock[i][0] / clock[i][1]),
				false);
			ctx.lineWidth = (canvas.height * (1 / clock.length) / 3);
			ctx.strokeStyle = (i % 2 == 0) ? "#336699" : "#FFCC66";
			ctx.stroke();
			ctx.closePath();

		}
				
	}, 500);
});


function get_circle_fraction( ratio ) {
	return (((ratio * 360) / 180 ) * Math.PI) - 0.5 * Math.PI;
}